 <?php 
  $body_margin='yes';
  $website='content-hub';
  $page='home';
  $channel='home';
  $login='no';  
  $share='no'; 
  $search_page='cari/'; 
  require ('inc/base.php')
?>
<?php require ($_SERVER['VIAHUB'].'inc/sample.php')?>
<?php require ($_SERVER['VIAHUB'].'inc/meta.php')?>
<?php require ($_SERVER['VIAHUB'].'inc/header.php')?>
<style>
  <?php require ($_SERVER['VIAHUB'].'css/slideshow-base.css')?>
  <?php require ($_SERVER['VIAHUB'].'css/slideshow-main.css')?>
  <?php require ($_SERVER['VIAHUB'].'css/slideshow-program.css')?>
  <?php require ($_SERVER['VIAHUB'].'css/slideshow-video.css')?>
  <?php require ($_SERVER['VIAHUB'].'css/slideshow-nikmati.css')?>
</style>
<script defer src="js/slideshow-base.js"></script>
<h1 class="hide">VIAHUB <?php echo $channel; ?></h1>
<div class="rancak-container">
  <span class="width-max">
    <div class="main-container">
	
	
	
	
	
	  <div class="column-full">
	    <div class="column-container">
		  <?php require ($_SERVER['VIAHUB'].'module/channel-carousel.php')?>
		</div>
	  </div>
	  
	  
	  
	  
	  
	  <div class="column-full column-full-nomargin">
	    <?php require ($_SERVER['VIAHUB'].'module/main-slideshow.php')?>
	  </div>
	
	
	
	
	
	  <div class="column-full">
	    <div class="column-container">
		  <section aria-label="Pilihan Top Untuk Kamu" class="section-container program-list">
		    <div class="section-title">
			  <div class="section-title-name">Pilihan Top Untuk Kamu</div>
			</div>
		    <div class="program-slideshow">
              <?php for ($i=1; $i <= 10 ; $i++) { ?>
                <?php 
                  $thumb_dimension='portrait'; $channel_name='Pesbukers'; $channel_link='pesbukers';
                  require ($_SERVER['VIAHUB'].'module/program-list.php')
				?>
              <?php } ?>
			</div>

			<script defer>
			$(document).ready(function(){
			  $('.program-slideshow').slick({
				prevArrow:'<button type="button" data-role="none" class="slick-nav slick-prev desktop-only"><span><img alt="Previous" src="img/icon/left.svg" width="10" height="18"/></span></button>',
				nextArrow:'<button type="button" data-role="none" class="slick-nav slick-next desktop-only"><span><img alt="Previous" src="img/icon/right.svg" width="10" height="18"/></span></button>',
				lazyLoad:'ondemand',
				dots:false,
				infinite:true,
				slidesToShow:5,
				slidesToScroll:1,
				autoplay:false,
				swipeToSlide:true,
				arrows:true,
				autoplaySpeed:5000,
				responsive:[
				  {
					breakpoint:1400,
					settings:{slidesToShow:4,}
				  },
				  {
					breakpoint:1024,
					settings:{slidesToShow:3,}
				  },
				  {
					breakpoint:640,
					settings:{slidesToShow:2,}
				  },
				  {
					breakpoint:360,
					settings:{slidesToShow:1,}
				  },
				],
			  });
			});
			</script>
		  </section>
		  
		  
		  
		  <section aria-label="Trending" class="section-container content-list">
		    <div class="section-title">
			  <div class="section-title-name">Trending</div>
			</div>
		    <div class="content-list-slideshow content-list-column">
              <?php for ($i=1; $i <= 10 ; $i++) { ?>
                <?php 
                  $content_live='no'; $show_channel='yes'; $show_date='yes'; $show_description='no'; $dateformat='ago'; 
                  $channel_name='Pesbukers'; $channel_link='pesbukers'; $play_now='no';
                  require ($_SERVER['VIAHUB'].'module/content-list.php')
				?>
              <?php } ?>
              <a aria-label="Link_Title" title="Link_Title" class="content-list-more content_center desktop-only" href="terbaru/">
                <div class="content-list-more-circle content_center">
				  <?php require ($_SERVER['VIAHUB'].'img/icon/right.svg')?>
				</div>
              </a>
			</div>
		  </section>
		</div>
	  </div>
	  
	  
	  
	  
	  
	  <div class="column-full column-full-nomargin">
	    <div class="column-container">
		  <?php require ($_SERVER['VIAHUB'].'module/nikmati-program.php')?>
		</div>
	  </div>
	  
	  
	  
	  
	  
	  <div class="column-full">
	    <div class="column-container">
		  <section aria-label="Episode Terbaru" class="section-container">
		    <div class="section-title">
			  <div class="section-title-name">Episode Terbaru</div>
			</div>
		    <div class="content-list-slideshow content-list-column">
              <?php for ($i=1; $i <= 10 ; $i++) { ?>
                <?php 
                  $content_live='no'; $show_channel='yes'; $show_date='yes'; $show_description='no'; $dateformat='ago'; 
                  $channel_name='Pesbukers'; $channel_link='pesbukers'; $play_now='no';
                  require ($_SERVER['VIAHUB'].'module/content-list.php')
				?>
              <?php } ?>
              <a aria-label="Link_Title" title="Link_Title" class="content-list-more content_center desktop-only" href="terbaru/">
                <div class="content-list-more-circle content_center">
				  <?php require ($_SERVER['VIAHUB'].'img/icon/right.svg')?>
				</div>
              </a>
			</div>
		  </section>
		  
		  
		  
		  <?php require ($_SERVER['VIAHUB'].'module/buat-pertunjukan-kamu.php')?>
		</div>
	  </div>
	  
	  
	  
	  
	  
    </div>
  </span>
</div>

<script defer>
$(document).ready(function(){
  $('.content-list-slideshow').slick({
	prevArrow:'<button type="button" data-role="none" class="slick-nav slick-prev desktop-only"><span><img alt="Previous" src="img/icon/left.svg" width="10" height="18"/></span></button>',
	nextArrow:'<button type="button" data-role="none" class="slick-nav slick-next desktop-only"><span><img alt="Previous" src="img/icon/right.svg" width="10" height="18"/></span></button>',
	lazyLoad:'ondemand',
	dots:false,
	infinite:true,
	slidesToShow:4,
	slidesToScroll:1,
	autoplay:false,
	swipeToSlide:true,
	arrows:true,
	autoplaySpeed:5000,
	responsive:[
	  {
		breakpoint:1360,
		settings:{slidesToShow:3,}
	  },
	  {
		breakpoint:640,
		settings:{slidesToShow:2,}
	  },
	  {
		breakpoint:360,
		settings:{slidesToShow:1,}
	  },
	],
  });
});
</script>

<?php require ($_SERVER['VIAHUB'].'inc/nav-bottom.php')?>
<?php require ($_SERVER['VIAHUB'].'inc/footer.php')?>
<?php require ($_SERVER['VIAHUB'].'inc/base-bottom.php')?>