 <?php 
  $body_margin='yes';
  $website='content-hub';
  $page='search';
  $channel='search';
  $login='no';  
  $share='no'; 
  $search_page='cari/'; 
  require ('../inc/base.php')
?>
<?php require ($_SERVER['VIAHUB'].'inc/sample.php')?>
<?php require ($_SERVER['VIAHUB'].'inc/meta.php')?>
<?php require ($_SERVER['VIAHUB'].'inc/header.php')?>
<div class="rancak-container">
  <span class="width-max">
    <div class="main-container">
	
	
	
	
	
	  <div class="column-full">
	    <div class="column-container">
		  <section aria-label="Hasil Pencarian" class="section-container search-page">
		    <?php require ($_SERVER['VIAHUB'].'module/search-box.php')?>
			<div class="search-note">Hasil pencarian <b><?php echo $random_channel[array_rand($random_channel)]; ?></b> tidak ditemukan. Silakan cari kata yang lain atau cek rekomendasi dibawah ini.</div>
		  </section>
		  
		  
		
		  <section aria-label="Rekomendasi" class="section-container content-list">
		    <div class="section-title">
			  <h1 class="section-title-name">Rekomendasi</h1>
			</div>
		    <div class="content-list-container content-list-column">
              <?php for ($i=1; $i <= 12 ; $i++) { ?>
                <?php 
                  $content_live='no'; $show_channel='yes'; $show_date='yes'; $show_description='no'; $dateformat='ago'; 
                  $channel_name='Pesbukers'; $channel_link='pesbukers'; $play_now='no';
                  require ($_SERVER['VIAHUB'].'module/content-list.php')
				?>
              <?php } ?>
			</div>
			<div class="muat-lainnya content_center">
			  <button title="Link_Title" class="muat-lainnya-button">Muat Lainnya</button>
			</div>
		  </section>
		</div>
	  </div>
	  
	  
	  
	  
	  
    </div>
  </span>
</div>
<?php require ($_SERVER['VIAHUB'].'inc/nav-bottom.php')?>
<?php require ($_SERVER['VIAHUB'].'inc/footer.php')?>
<?php require ($_SERVER['VIAHUB'].'inc/base-bottom.php')?>