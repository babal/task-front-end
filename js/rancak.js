function clear_popup(){     
  $('.rancak-popup').slideUp('fast');
  $('.open-sticky').removeClass('show-sticky');
;}



/* scroll script */
var all_scroll = function(){
  "use strict";	

  $(window).scroll(function(){
    var scroll_position = $(window).scrollTop();

    if(scroll_position >= 1){
      $('header').addClass('header-sticky');	
    }

    else{
      $('header').removeClass('header-sticky');
    }
  });  
};
/* end scroll script */



var channel_carousel_open = function(){
  "use strict";
  $('.channel-button-showall').click(function(){
    $(this).hide();
	$('.channel-list-container').addClass('channel-carousel-open');
  });
};

var open_sticky = function(){
  "use strict";
  $('.open-sticky').click(function(){
    var get_id = $(this).attr('title');
	$('.sticky-sub').removeClass('sticky-sub-show');
	$('.open-sticky[title=' + get_id +']').toggleClass('show-sticky');
	$('.open-sticky').not('.open-sticky[title=' + get_id +']').removeClass('show-sticky');
    $('#float-' + get_id).slideToggle('fast');
    $('.rancak-popup').not('#float-' + get_id).slideUp('fast');
	if($('#float-' + get_id).hasClass("popup-alert-temporary")){
      setTimeout(function() {
        clear_popup();
      },2000);
	};
    return false;
  });	
};



var popup_share = function(){
  "use strict";
  $('.popup-full-overlay, .popup-full-close').click(function(){
	clear_popup();
	return false;
  });
};



var open_menu = function(){
  "use strict";
  $('.sticky-box-profile > .sticky-link-profile').click(function(){
	$(this).parent().find('.sticky-sub').toggleClass('sticky-sub-show');
	clear_popup();
	return false;
  });
};



var toggle_bookmark = function(){
  "use strict";
  $('.toggle-bookmark').click(function(){
    var get_label = $(this).attr('data-title');
	if(get_label == 'Add'){
	  $(this).attr("data-title","Remove");
	  $('.popup-toggle-bookmark').find('.popup-alert-box').html('Bookmark berhasil ditambah');
	};
	if(get_label == 'Remove'){
	  $(this).attr("data-title","Add");
	  $('.popup-toggle-bookmark').find('.popup-alert-box').html('Bookmark berhasil dihapus');
	};
	return false;
  });
};



var close_popup_small = function(){
  "use strict";
  $('.popup-alert-close').click(function(){
	$('.rancak-popup').slideUp('fast');
	$('.open-sticky').removeClass('show-sticky');
	return false;
  });
};



function CopyURL(text) {
  var inputc = document.body.appendChild(document.createElement("input"));
  inputc.value = window.location.href;
  inputc.focus();
  inputc.select();
  document.execCommand('copy');
  inputc.parentNode.removeChild(inputc);
  $('.popup-copied').slideDown('fast').delay(2000).slideUp('fast');
};



function CopyLink() {
  var copyText = document.getElementById("CopyLinkField");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
}



$(document).ready(function(){
  "use strict";
  all_scroll();
  channel_carousel_open();
  open_menu();
  open_sticky();
  popup_share();
  toggle_bookmark();
  close_popup_small();
});