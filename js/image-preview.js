$(".profile-upload").change(function() {
  if (this.files && this.files[0]){
    $location = $(this);
    var reader = new FileReader();
    reader.onload = function(e) {
      $location.parent().css('background-image', 'url('+e.target.result +')');
      $location.parent().find('img').fadeOut(650);
    }
    reader.readAsDataURL(this.files[0]);
  }
});