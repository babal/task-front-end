/* scroll detail */
var detail_scroll = function(){
  "use strict";	

  $(window).scroll(function(){
    var scroll_position = $(window).scrollTop();
    var player_position_down = $('.video-player').position().top + $('.video-player').height();

    if(scroll_position >= player_position_down){
      $('.video-player').addClass('video-sticky');	
    }

    else{
      $('.video-player').removeClass('video-sticky');	
    }
  });  
};
/* end scroll detail */



var toggle_comment_sort = function(){
  "use strict";
  $('.comment-head-sort').click(function(){
    $(this).toggleClass("comment-head-sort-ac comment-head-sort-dc");
	return false;
  });
};



var toggle_comment_reply = function(){
  "use strict";
  $('.cal-reply').click(function(){
    $(this).toggleClass("cal-reply-open");
    $(this).parents().eq(1).find('.comment-form').slideToggle("fast");
	return false;
  });
};



var report_received = function(){
  "use strict";
  $('.popup-report-button').click(function(){
    $('.popup-report-box').scrollTop(0);
    $('.popup-report-box').css("overflow","hidden");
    $('.popup-report-received').addClass("popup-report-received-show");
	return false;
  });
};



$(document).ready(function(){
  "use strict";
  //detail_scroll();
  toggle_comment_sort();
  toggle_comment_reply();
  report_received();
});