<div class="information-menu mobile-only">
  <select class="information-dropdown" onchange="location=this.value;">
  <?php 
    foreach($information_array as $information_array_list){
  ?>
    <option value="informasi/<?php echo($information_array_list['information_link']) ?>.php"
    <?php if($channel == $information_array_list['information_link']) { ?>selected<?php } ?>>
      <?php echo($information_array_list['information_label']) ?>
    </option>
  <?php } ?>
  </select>
  <div class="information-dropdown-icon content_center">
    <?php require ($_SERVER['VIAHUB'].'img/icon/dropdown.svg')?>
  </div>
</div>



<div class="column-left column-small desktop-only">
  <div class="column-container column-sticky">
    <nav class="section-container information-menu">
      <?php 
        foreach($information_array as $information_array_list){
      ?>
      <a aria-label="<?php echo($information_array_list['information_label']) ?>" title="<?php echo($information_array_list['information_label']) ?>" 
      href="informasi/<?php echo($information_array_list['information_link']) ?>.php" 
      class="information-menu-link <?php if($channel == $information_array_list['information_link']) { ?>information-menu-curr<?php } ?>">
        <?php echo($information_array_list['information_label']) ?>
      </a>
      <?php } ?>
    </nav>
  </div>
</div>