<div class="rancak-popup popup-full popup-share hide" id="float-Share">
  <div class="popup-full-overlay"></div>
  <div class="popup-full-container popup-share-container">
    <div class="popup-full-box popup-share-box">
	  <div class="popup-share-head">
	    <div class="popup-share-title">Bagikan ke Teman Kamu</div>
	    <button class="popup-full-close popup-share-close"><?php require ($_SERVER['VIAHUB'].'img/icon/close.svg')?></button>
	  </div>
	  <div class="popup-share-list">
	    <a aria-label="Facebook" title="Facebook" class="popup-share-link popup-share-facebook content_center" href="">
		  <?php require ($_SERVER['VIAHUB'].'img/icon/facebook-circle.svg')?>
		</a>
	    <a aria-label="Twitter" title="Twitter" class="popup-share-link popup-share-twitter content_center" href="">
		  <?php require ($_SERVER['VIAHUB'].'img/icon/twitter-circle.svg')?>
		</a>
	    <a aria-label="Whatsapp" title="Whatsapp" class="popup-share-link popup-share-whatsapp content_center" href="">
		  <?php require ($_SERVER['VIAHUB'].'img/icon/whatsapp-circle.svg')?>
		</a>
	  </div>
	  <div class="popup-share-copyurl">
	    <input type="text" class="psc-field" id="CopyLinkField" value="https://www.viahub.id/video/tawa-sutra/735-tawa-sutra-10">
		<button title="Copy URL" class="psc-button" onclick="CopyLink()">
		  <?php require ($_SERVER['VIAHUB'].'img/icon/copy-link.svg')?>
		  <span>Salin</span>
		</button>
	  </div>
	</div>
  </div>
</div>



<div class="rancak-popup popup-search hide" id="float-Cari">
  <?php require ($_SERVER['VIAHUB'].'module/search-box.php')?>
</div>



<div class="rancak-popup popup-alert popup-alert-temporary popup-toggle-bookmark hide" id="float-Bookmark">
  <div class="popup-alert-box"></div>
</div>



<noscript id="deferred-styles">
  <link rel="stylesheet" type="text/css" href="css/hold.css?<?php echo $anticache; ?>" media="print" onload="this.media='all'"/>
</noscript>
<script defer>
  var loadDeferredStyles = function() {
	var addStylesNode = document.getElementById("deferred-styles");
	var replacement = document.createElement("div");
	replacement.innerHTML = addStylesNode.textContent;
	document.body.appendChild(replacement)
	addStylesNode.parentElement.removeChild(addStylesNode);
  };
  var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
	  window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
  if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
  else window.addEventListener('load', loadDeferredStyles);
</script>

<script defer src="js/lazysizes.min.js"></script>
<script defer rancak-hold="js/rancak.js"></script>



</body>
</html>