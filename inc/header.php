<header class="content_center">
  <span class="width-max">
  
	<div class="header-left">
      <a aria-label="Home" title="Home" class="sticky-link sticky-link-logo content_center" href="index.php">
        <picture>
          <source srcset="img/logo-viahub.webp" type="image/webp">
          <source srcset="img/logo-viahub.png">
          <img alt="Logo VIAHub" src="img/logo-viahub.png" width="96.66" height="24">
        </picture>
      </a>
      <a aria-label="Content Hub" title="Content Hub" href="index.php"
	  class="sticky-link <?php if($website == 'content-hub') { ?>sticky-curr<?php } ?> content_center desktop-only">
        <span class="sticky-label">Content Hub</span>
      </a>
      <a aria-label="Talent Hub" title="Talent Hub" href="talent-hub/"
	  class="sticky-link <?php if($website == 'talent-hub') { ?>sticky-curr<?php } ?> content_center desktop-only">
        <span class="sticky-label">Talent Hub</span>
      </a>
	</div>
	
	
	
	<div class="header-right">
      <div class="sticky-box">
        <button title="Cari" class="sticky-link open-sticky content_center desktop-only">
          <?php require ($_SERVER['VIAHUB'].'img/icon/search.svg')?>
        </button>
	  </div>
	  <div class="sticky-box <?php if($login == 'yes') { ?>sticky-box-profile<?php } ?>">
        <a aria-label="Profile" title="Profile" class="sticky-link sticky-link-profile content_center" 
          <?php if($login == 'no') { ?>
            href="profile/login.php"
          <?php } ?> 
          <?php if($login == 'yes') { ?>
            href=""
          <?php } ?>
        >
          <div class="slp-circle flex_ori thumb-loading">
            <img alt="img_title" class="lazyload" 
              <?php if($login == 'no') { ?>
                data-original="img/icon-profile-small.png"
              <?php } ?>
              <?php if($login == 'yes') { ?>
                data-original="img/sample/icon-profile-small-<?php echo rand(1,5); ?>.jpg"
              <?php } ?>
            />
          </div>
        </a>
		
		<?php if($login == 'yes') { ?>
          <div class="sticky-sub hide">
            <div class="sticky-sub-container">
              <a aria-label="Profil" title="Profil" class="sticky-link-sub" href="profile/">
                <?php require ($_SERVER['VIAHUB'].'img/icon/profile.svg')?>
                <div class="sticky-link-sub-label">Profil</div>
              </a>
              <a aria-label="Bookmark" title="Bookmark" class="sticky-link-sub" href="bookmark/">
                <?php require ($_SERVER['VIAHUB'].'img/icon/bookmark.svg')?>
                <div class="sticky-link-sub-label">Bookmark</div>
              </a>
              <a aria-label="Logout" title="Logout" class="sticky-link-sub" href="index.php">
                <?php require ($_SERVER['VIAHUB'].'img/icon/logout.svg')?>
                <div class="sticky-link-sub-label">Logout</div>
              </a>
            </div>
          </div>
		<?php } ?>
	  </div>
	</div>
	
  </span>
</header>