<nav class="mobile-only">
  <a aria-label="Content Hub" title="Content Hub" href="index.php"
  class="sticky-link <?php if($website == 'content-hub') { ?>sticky-curr<?php } ?> content_center">
    <?php require ($_SERVER['VIAHUB'].'img/icon/content-hub.svg')?>
	<span class="sticky-label">Content Hub</span>
  </a>
  <a aria-label="Talent Hub" title="Talent Hub" class="sticky-link content_center" href="talent-hub/"
  class="sticky-link <?php if($website == 'talent-hub') { ?>sticky-curr<?php } ?> content_center">
    <?php require ($_SERVER['VIAHUB'].'img/icon/talent-hub.svg')?>
	<span class="sticky-label">Talent Hub</span>
  </a>
  <button title="Cari" class="sticky-link open-sticky content_center">
	<?php require ($_SERVER['VIAHUB'].'img/icon/search.svg')?>
	<span class="sticky-label">Cari</span>
  </button>
</nav>