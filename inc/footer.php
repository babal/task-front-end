<footer class="content_center">
  <span class="width-max">
  
	<div class="footer-socmed">
	  <span>Follow us</span>
	  <a aria-label="Facebook" alt="Facebook" class="footer-socmed-link flex_ori thumb-loading" rel="noopener" target="_blank"
	  href="https://www.facebook.com/DBSentertainment">
	    <img alt="Facebook" class="lazyload" data-original="img/icon/facebook.svg" />
	  </a>
	  <a aria-label="Instagram" alt="Instagram" class="footer-socmed-link" rel="noopener" target="_blank"
	  href="https://www.instagram.com/viacreativehub/">
	    <img alt="Instagram" class="lazyload" data-original="img/icon/instagram.svg" />
	  </a>
	  <a aria-label="Twitter" alt="Twitter" class="footer-socmed-link" rel="noopener" target="_blank"
	  href="">
	    <img alt="Twitter" class="lazyload" data-original="img/icon/twitter.svg" />
	  </a>
	  <a aria-label="Youtube" alt="Youtube" class="footer-socmed-link" rel="noopener" target="_blank"
	  href="https://www.youtube.com/channel/UCxxbUAW57siweehySRUgJcA">
	    <img alt="Youtube" class="lazyload" data-original="img/icon/youtube.svg" />
	  </a>
	  <a aria-label="Linkedin" alt="Linkedin" class="footer-socmed-link" rel="noopener" target="_blank"
	  href="https://id.linkedin.com/company/viayourcreativehub">
	    <img alt="Linkedin" class="lazyload" data-original="img/icon/linkedin.svg" />
	  </a>
	</div>
	
	
  
	<div class="footer-menu">
	  <a aria-label="Peta Situs" alt="Peta Situs" class="footer-menu-link" href="informasi/peta-situs.php">
	    Peta Situs
	  </a>
	  <a aria-label="Tentang Kami" alt="Tentang Kami" class="footer-menu-link" href="informasi/tentang-kami.php">
	    Tentang Kami
	  </a>
	  <a aria-label="Kontak" alt="Kontak" class="footer-menu-link" href="informasi/kontak.php">
	    Kontak
	  </a>
	  <a aria-label="Info Iklan" alt="Info Iklan" class="footer-menu-link" href="informasi/info-iklan.php">
	    Info Iklan
	  </a>
	  <a aria-label="Pedoman Media Siber" alt="Pedoman Media Siber" class="footer-menu-link" href="informasi/pedoman-media-siber.php">
	    Pedoman Media Siber
	  </a>
	  <a aria-label="Kebijakan Privasi" alt="Panduan Kebijakan" class="footer-menu-link" href="informasi/kebijakan-privasi.php">
	    Kebijakan Privasi
	  </a>
	  <a aria-label="Disclaimer" alt="Disclaimer" class="footer-menu-link" href="informasi/disclaimer.php">
	    Disclaimer
	  </a>
	  <a aria-label="Karir" alt="Karir" class="footer-menu-link" href="informasi/karir.php" target="_blank">
	    Karir
	  </a>
	</div>
  
  
  
	<div class="footer-copyright">
	  <span>VIA &copy;2022 - All Right Reserved</span>
	</div>
	
  </span>
</footer>