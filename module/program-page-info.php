<section aria-label="Tonton Sekarang" class="section-container program-page-action">
  <a aria-label="Link_Title" title="Link_Title" class="program-page-continue" href="<?php echo $channel; ?>/detail.php">
    <div class="ppc-icon"><?php require ($_SERVER['VIAHUB'].'img/icon/play-circle.svg')?></div>
    <div class="ppc-label"><span>Putar Sekarang</span></div>
  </a>
  <button title="Bookmark" data-title="Add" class="open-sticky program-page-button toggle-bookmark">
    <?php require ($_SERVER['VIAHUB'].'img/icon/bookmark-outline.svg')?>
    <?php require ($_SERVER['VIAHUB'].'img/icon/bookmark.svg')?>
  </button>
  <button title="Share" class="open-sticky program-page-button">
    <?php require ($_SERVER['VIAHUB'].'img/icon/share.svg')?>
  </button>
</section>



<?php if($program_page_type == 'multiple') { ?>
  <section aria-label="Daftar Episode" class="section-container content-list">
    <div class="section-title">
      <h1 class="section-title-name">Daftar Episode</h1>
    </div>
    <div class="content-list-container content-list-row">
      <?php for ($i=1; $i <= 12 ; $i++) { ?>
        <?php 
          $content_live='no'; $show_channel='no'; $show_date='no'; $show_description='yes'; $dateformat='ago'; 
          $channel_name='Pesbukers'; $channel_link='pesbukers'; $play_now='no';
          require ($_SERVER['VIAHUB'].'module/content-list.php')
        ?>
      <?php } ?>
    </div>
    <div class="muat-lainnya content_center">
      <button title="Link_Title" class="muat-lainnya-button">Muat Lainnya</button>
    </div>
  </section>
<?php } ?>



<section aria-label="Rekomendasi Lainnya" class="section-container program-list">
  <div class="section-title">
    <div class="section-title-name">Rekomendasi Lainnya</div>
  </div>
  <div class="program-list-container">
    <?php for ($i=1; $i <= 3 ; $i++) { ?>
      <?php 
        $thumb_dimension='portrait'; $channel_name='Pesbukers'; $channel_link='pesbukers';
        require ($_SERVER['VIAHUB'].'module/program-list.php')
      ?>
    <?php } ?>
    <?php 
      $thumb_dimension='portrait'; $channel_name='Pesbukers'; $channel_link='pesbukers/single.php';
      require ($_SERVER['VIAHUB'].'module/program-list.php')
    ?>
  </div>
</section>