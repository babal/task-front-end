<a aria-label="Link_Title" title="Link_Title" 
class="channel-button 
<?php if($channel_link == 'live') { ?>channel-button-live<?php } ?> <?php if($channel_custom == 'curr') { ?>channel-button-curr<?php } ?>" 
href="<?php echo $channel_link; ?>/">
  <div class="channel-button-frame content_center">
    <span>
      <?php if($channel_name == 'random') { ?>
        <?php echo $random_channel[array_rand($random_channel)]; ?>
      <?php } ?>
      <?php if($channel_name != 'random') { ?>
        <?php echo $channel_name; ?>
      <?php } ?>
    </span>
  </div>
</a>