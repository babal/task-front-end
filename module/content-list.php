<a aria-label="Link_Title" title="Link_Title" href="<?php echo $channel_link; ?>/detail.php" 
class="content-list-box <?php if($content_live == 'yes' || $content_live == 'coming-soon') { ?>content-live-box<?php } ?>
<?php if($channel == 'bookmark') { ?>content-list-action<?php } ?>
<?php if($play_now == 'yes') { ?>content-play-now<?php } ?>">
  <div class="content-list-thumb">
    <div class="content-list-thumb-frame flex_ori thumb-loading">
      <img alt="img_title" class="lazyload" data-original="img/sample/sample-<?php echo rand(1,20); ?>.jpg" />
	  <?php if($play_now == 'no') { ?>
        <div class="content-list-thumb-icon hide content_center">
          <?php require ($_SERVER['VIAHUB'].'img/icon/play-circle.svg')?>
        </div>
	  <?php } ?>
	  <?php if($play_now == 'yes') { ?>
        <div class="content-list-thumb-icon play-now-icon content_center">
          <?php require ($_SERVER['VIAHUB'].'img/icon/play-circle.svg')?>
		  <span>Sedang diputar</span>
        </div>
	  <?php } ?>
    </div>
  </div>
  
  
  <div class="content-list-info">
    <span>
	  <h2 class="content-list-title"><?php echo $random_title[array_rand($random_title)]; ?></h2>
	  
      <?php if($show_channel == 'yes') { ?>
        <h3 class="content-list-channel"><?php echo $random_channel[array_rand($random_channel)]; ?></h3>
      <?php } ?>
	  
      <?php if($show_date == 'yes') { ?>
        <div class="content-list-date">
          <?php if($dateformat == 'ago') { ?>1 menit yang lalu<?php } ?>
          <?php if($dateformat == 'min') { ?>00 Sep 0000<?php } ?>
          <?php if($dateformat == 'full') { ?>00 Sep 0000 - 00:00 WIB<?php } ?>
        </div>
      <?php } ?>
	  
      <?php if($show_description == 'yes') { ?>
        <div class="content-list-desc"><?php echo $random_desc[array_rand($random_desc)]; ?></div>
      <?php } ?>
	  
      <?php if($content_live == 'yes' || $content_live == 'coming-soon') { ?>
        <div class="content-live-info">
		  <div class="live-misc-box content-live-row">
		    <div class="lmb-icon"><?php require ($_SERVER['VIAHUB'].'img/icon/calendar.svg')?></div>
		    <div class="lmb-label">Senin, 00 Sep 0000</div>
		  </div>
		  <div class="live-misc-box content-live-row">
		    <div class="lmb-icon"><?php require ($_SERVER['VIAHUB'].'img/icon/time.svg')?></div>
		    <div class="lmb-label">00:00 - 00:00</div>
		  </div>
		  <div class="live-misc-box content-live-row">
		    <div class="lmb-icon"><?php require ($_SERVER['VIAHUB'].'img/icon/flag.svg')?></div>
		    <div class="lmb-label">Free</div>
		  </div>
		</div>
		
		<div class="content-live-action">
		  <button title="Tonton Live Streaming" class="content-live-button">
		    <?php if($content_live == 'yes') { ?>
			  <?php require ($_SERVER['VIAHUB'].'img/icon/play.svg')?>
			  <span>Putar Sekarang</span>
			<?php } ?>
		    <?php if($content_live == 'coming-soon') { ?>
			  <?php require ($_SERVER['VIAHUB'].'img/icon/play.svg')?>
			  <span>Coming Soon</span>
			<?php } ?>
		  </button>
		  <button title="Share" class="open-sticky content-live-share popup-share-button content_center">
		    <?php require ($_SERVER['VIAHUB'].'img/icon/share.svg')?>
		  </button>
		</div>
      <?php } ?>
	</span>
  </div>
  
  <?php if($channel == 'bookmark') { ?>
    <div class="content-delete-bookmark">
	  <button title="HapusBookmark" class="open-sticky content-delete-bookmark-button">
	    <?php require ($_SERVER['VIAHUB'].'img/icon/close.svg')?>
	  </button>
	</div>
  <?php } ?>
</a>