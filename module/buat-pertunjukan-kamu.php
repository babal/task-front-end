<section aria-label="Buat Pertunjukan Kamu" class="section-container buat-pertunjukan-kamu">
  <div class="buat-pertunjukan-kamu-container">
    <div class="bpkc-bg flex_ori thumb-loading">
	  <img alt="Buat Pertunjukan Kamu" class="lazyload" data-original="img/cover-default-2.jpg"
      data-srcset="img/cover-default-2.jpg 1281w, img/cover-default-2-full.jpg 1920w" />
	</div>
    <div class="bpkc-content">
	  <span>
	    <div class="bpkc-title">Buat Pertunjukan Kamu</div>
	    <div class="bpkc-desc"><?php echo $random_desc[array_rand($random_desc)]; ?></div>
	    <div class="bpkc-submit">
		  <a aria-label="Hubungi Kami" title="Hubungi Kami" class="btn bpkc-button" href="informasi/kontak.php">
		    Hubungi Kami
		  </a>
		</div>
	  </span>
	</div>
  </div>
</section>

<noscript id="buat-pertunjukan-kamu-styles">
  <link rel="stylesheet" type="text/css" href="css/buat-pertunjukan-kamu.css?<?php echo $anticache; ?>" media="print" onload="this.media='all'"/>
</noscript>
<script defer>
  var BuatPertunjukanKamuStyles = function() {
	var addStylesNode = document.getElementById("buat-pertunjukan-kamu-styles");
	var replacement = document.createElement("div");
	replacement.innerHTML = addStylesNode.textContent;
	document.body.appendChild(replacement)
	addStylesNode.parentElement.removeChild(addStylesNode);
  };
  var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
	  window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
  if (raf) raf(function() { window.setTimeout(BuatPertunjukanKamuStyles, 0); });
  else window.addEventListener('load', BuatPertunjukanKamuStyles);
</script>