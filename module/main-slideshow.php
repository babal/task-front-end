<section aria-label="Program Highlight" class="section-container slideshow-main">
  <div class="slideshow-main-frame"><div></div></div>
  <div class="slideshow-main-container">
    <?php for ($i=1; $i <= 8 ; $i++) { ?>
      <?php 
        $thumb_dimension='landscape'; $channel_name='Pesbukers'; $channel_link='pesbukers';
        require ($_SERVER['VIAHUB'].'module/program-list.php')
      ?>
    <?php } ?>
  </div>
</section>



<script defer>
$(document).ready(function(){
  $('.slideshow-main-container').slick({
	prevArrow:'<button type="button" data-role="none" class="slick-nav slick-prev desktop-only"><span><img alt="Previous" src="img/icon/left.svg" width="10" height="18"/></span></button>',
	nextArrow:'<button type="button" data-role="none" class="slick-nav slick-next desktop-only"><span><img alt="Previous" src="img/icon/right.svg" width="10" height="18"/></span></button>',
	lazyLoad:'ondemand',
	dots:false,
	infinite:true,
	slidesToShow:3,
	slidesToScroll:1,
	autoplay:true,
	swipeToSlide:true,
	arrows:true,
	autoplaySpeed:5000,
	responsive:[
	  {
	    breakpoint:1920,
	    settings:{slidesToShow:2,}
      },
	  {
	    breakpoint:1280,
	    settings:{slidesToShow:1,}
      },
    ],
  });
});
</script>