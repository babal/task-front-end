
<section aria-label="Nikmati Program" class="section-container nikmati-program content_center">
  <div class="nikmati-program-container">
    <div class="npc-bg flex_ori thumb-loading">
	  <img alt="Buat Pertunjukan Kamu" class="lazyload" data-original="img/cover-default-1.jpg"
      data-srcset="img/cover-default-1.jpg 1281w, img/cover-default-1-full.jpg 1920w" />
	</div>
	
	<div class="npc-content">
	  <div class="program-list">
		<div class="section-title">
		  <div class="section-title-name">Nikmati Program</div>
		</div>
		<div class="nikmati-program-slideshow">
		  <?php for ($i=1; $i <= 10 ; $i++) { ?>
			<?php 
			  $thumb_dimension='portrait'; $channel_name='Pesbukers'; $channel_link='pesbukers';
			  require ($_SERVER['VIAHUB'].'module/program-list.php')
			?>
		  <?php } ?>
		</div>
	  </div>
	</div>
  </div>
</section>

<noscript id="nikmati-program-styles">
  <link rel="stylesheet" type="text/css" href="css/nikmati-program.css?<?php echo $anticache; ?>" media="print" onload="this.media='all'"/>
</noscript>
<script defer>
  var NikmatiProgramStyles = function() {
	var addStylesNode = document.getElementById("nikmati-program-styles");
	var replacement = document.createElement("div");
	replacement.innerHTML = addStylesNode.textContent;
	document.body.appendChild(replacement)
	addStylesNode.parentElement.removeChild(addStylesNode);
  };
  var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
	  window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
  if (raf) raf(function() { window.setTimeout(NikmatiProgramStyles, 0); });
  else window.addEventListener('load', NikmatiProgramStyles);
</script>

<script defer>
$(document).ready(function(){
  $('.nikmati-program-slideshow').slick({
	prevArrow:'<button type="button" data-role="none" class="slick-nav slick-prev desktop-only"><span><img alt="Previous" src="img/icon/left.svg" width="10" height="18"/></span></button>',
	nextArrow:'<button type="button" data-role="none" class="slick-nav slick-next desktop-only"><span><img alt="Previous" src="img/icon/right.svg" width="10" height="18"/></span></button>',
	lazyLoad:'ondemand',
	dots:false,
	infinite:true,
	slidesToShow:4,
	slidesToScroll:1,
	autoplay:false,
	swipeToSlide:true,
	arrows:true,
	autoplaySpeed:5000,
	responsive:[
	  {
	    breakpoint:1600,
	    settings:{slidesToShow:3,}
      },
	  {
	    breakpoint:1280,
	    settings:{slidesToShow:2,}
      },
	  {
	    breakpoint:1024,
	    settings:{slidesToShow:3,}
      },
	  {
	    breakpoint:640,
	    settings:{slidesToShow:2,}
      },
	  {
	    breakpoint:360,
	    settings:{slidesToShow:1,}
      },
    ],
  });
});
</script>