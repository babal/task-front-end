<style><?php require ($_SERVER['VIAHUB'].'css/live-headline.css')?></style>
<section aria-label="Live Headline" class="live-headline">
  <a aria-label="Link_Title" title="Link_Title" class="live-headline-cover flex_ori thumb-loading" href="live/detail.php">
    <img alt="img_title" class="lazyload" data-original="img/cover-default-1-full.jpg" />
  </a>
  <div class="live-headline-info">
    <div class="live-headline-date">
	  <div class="live-headline-icon">
	    <?php require ($_SERVER['VIAHUB'].'img/icon/calendar.svg')?>
	  </div>
	  <div class="live-headline-label">
	    Senin, 00 September 0000
	  </div>
	</div>
    <div class="live-headline-date">
	  <div class="live-headline-icon">
	    <?php require ($_SERVER['VIAHUB'].'img/icon/time.svg')?>
	  </div>
	  <div class="live-headline-label">
	    00:00 - 00:00
	  </div>
	</div>
	<a aria-label="Tonton Sekarang" title="Tonton Sekarang" class="live-headline-title" href="live/detail.php">
	  <h2><?php echo $random_title[array_rand($random_title)]; ?></h2>
	</a>
	<div class="content-live-action live-headline-action">
	  <a aria-label="Tonton Sekarang" title="Tonton Sekarang" class="content-live-button" href="live/detail.php">
        <?php require ($_SERVER['VIAHUB'].'img/icon/play.svg')?>
        <span>Tonton Sekarang</span>
	  </a>
	  <button title="Share" class="open-sticky content-live-share popup-share-button content_center">
		<?php require ($_SERVER['VIAHUB'].'img/icon/share.svg')?>
	  </button>
	</div>
  </div>
</section>