<div class="program-list-box">
  <a aria-label="Link_Title" title="Link_Title" class="program-list-thumb program-list-thumb-<?php echo $thumb_dimension; ?> flex_ori thumb-loading" 
  href="<?php echo $channel_link; ?>/">
    <img alt="img_title" class="lazyload" data-original="img/poster-<?php echo $thumb_dimension; ?>-<?php echo $random_poster[array_rand($random_poster)]; ?>.jpg" />
  </a>
  
  <div class="program-list-info desktop-only">
    <a aria-label="Link_Title" title="Link_Title" class="program-list-overlay" href="<?php echo $channel_link; ?>/"></a>
    <span>
	  <div class="program-list-nav">
	    <a aria-label="Link_Title" title="Link_Title" class="program-list-button content_center" href="<?php echo $channel_link; ?>/detail.php">
          <?php require ($_SERVER['VIAHUB'].'img/icon/play.svg')?>
        </a>
	    <a aria-label="Link_Title" title="Link_Title" class="program-list-button content_center" href="<?php echo $channel_link; ?>/">
          <?php require ($_SERVER['VIAHUB'].'img/icon/list.svg')?>
        </a>
	  </div>
	  <a aria-label="Link_Title" title="Link_Title" class="program-list-title" href="<?php echo $channel_link; ?>/">
	    <h2><?php echo $random_channel[array_rand($random_channel)]; ?></h2>
	  </a>
	  <a aria-label="Link_Title" title="Link_Title" class="program-list-episode" href="<?php echo $channel_link; ?>/">
	    <?php echo rand(1,20); ?> Episode
	  </a>
	</span>
  </div>
</div>